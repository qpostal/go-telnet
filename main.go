package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	flag "github.com/spf13/pflag"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

var timeoutArg = flag.String("timeout", "10s", "timeout in seconds")

func main() {
	flag.Parse()

	if flag.NArg() != 2 {
		log.Printf("usage %s host port\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	host := flag.Arg(0)
	port := flag.Arg(1)

	timeoutStr := strings.Replace(*timeoutArg, "s", "", 1)
	timeout, err := strconv.Atoi(timeoutStr)
	if err != nil {
		log.Fatalf("timeout argument: %v\n", err)
	}

	ctxTimeout, cancelTimeout := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)
	defer cancelTimeout()

	var d net.Dialer
	conn, err := d.DialContext(ctxTimeout, "tcp", fmt.Sprintf("%s:%s", host, port))
	if err != nil {
		log.Fatalf("connection: %v\n", err)
	}

	defer conn.Close()

	wg := sync.WaitGroup{}
	wg.Add(2)

	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		defer wg.Done()
		defer cancel()

		for {
			scanner := bufio.NewScanner(os.Stdin)
			if scanner.Scan() {
				_, err = conn.Write(scanner.Bytes())

				if err != nil {
					if errors.Is(err, syscall.EPIPE) {
						log.Println("connection closed")
					} else {
						log.Printf("connection write: %v\n", err)
					}
					return
				}
			} else {
				log.Println("stopped")
				return
			}
		}
	}()

	ch := make(chan string)

	go func() {
		for {
			scanner := bufio.NewScanner(conn)
			if scanner.Scan() {
				ch <- scanner.Text()
			}
		}
	}()

	go func() {
		defer wg.Done()

		for {
			select {
			case text := <-ch:
				fmt.Println(text)
			case <-ctx.Done():
				return
			}
		}
	}()

	wg.Wait()
}
